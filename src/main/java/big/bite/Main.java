package big.bite;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {

        ExecutorService service = Executors.newFixedThreadPool(10);

        AtomicInteger counter1 = new AtomicInteger(0);
        AtomicInteger counter2 = new AtomicInteger(0);

        int numOfThreads = 100_000;

        for (int i = 0; i < numOfThreads; i++) {
            service.submit(() -> {
                counter1.incrementAndGet();
                counter2.incrementAndGet();
            });
        }
        service.shutdown();

        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Counter_1: " + counter1.get());
        System.out.println("Counter_2: " + counter2.get());

    }
}